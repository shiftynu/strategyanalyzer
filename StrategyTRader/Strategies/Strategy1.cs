﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CoinbasePro.WebSocket.Models.Response;
using Serilog;

namespace StrategyTrader.Strategies
{
    public class Strategy1 : StrategyBase
    {
        private Ticker _lastBuyTicker;
        private Ticker _lastSellTicker;
        private Ticker _lastCheckTicker;
        private double _waitSeconds = 30;
        private bool _readyForBuy = true;
        private decimal _buyClimbThreshold = 1m;
        private decimal _waitingForSaleMaxPrice = 0.0m;
        private decimal _sellWalkbackPercentThreshold = 0.9m;

        public Strategy1(int waitSeconds, decimal buyClimbThreshold, decimal sellWalkbackPercentThreshold)
        {
            Name = $"S1 {waitSeconds} s | bc: {buyClimbThreshold} | swb %: {sellWalkbackPercentThreshold}";
            _waitSeconds = waitSeconds;
            _buyClimbThreshold = buyClimbThreshold;
            _sellWalkbackPercentThreshold = sellWalkbackPercentThreshold;
        }

        public override void ExecuteStrategy(Ticker ticker)
        {
            //LogInfo($"{ticker.Time} {ticker.Price} {ticker.BestBid} {ticker.BestAsk}");
            if (_lastCheckTicker == null)
            {
                FirstTicker = ticker;
                _lastCheckTicker = ticker;
            }
            else
            {
                // update highest price after buy
                if (!_readyForBuy && ticker.Price > _waitingForSaleMaxPrice) _waitingForSaleMaxPrice = ticker.Price;
                if (ticker.Time.AddSeconds(-_waitSeconds) >= _lastCheckTicker.Time)
                {
                    // check
                    if (_readyForBuy)
                    {
                        LogInfo("Checking buy at {@Price1} against last check at {@Price2}...been {@BeenSecs} secs", ticker.Price, _lastCheckTicker.Price, (ticker.Time - _lastCheckTicker.Time).TotalSeconds);
                        if (_lastSellTicker != null)
                            TotalWaitSecs += (ticker.Time - _lastSellTicker.Time).TotalSeconds;
                        else
                            TotalWaitSecs += (ticker.Time - _lastCheckTicker.Time).TotalSeconds;
                    }
                    else
                    {
                        LogInfo("Checking sale at {@Price1} for bought at {@Price2}...been {@BeenSecs} secs", ticker.Price, _lastBuyTicker.Price, (ticker.Time - _lastCheckTicker.Time).TotalSeconds);
                    }
                    if (_readyForBuy && ticker.Price > _lastCheckTicker.Price + _buyClimbThreshold)
                    {
                        // BUY
                        LogWarn("Buy at {@Price}", ticker.Price);
                        _waitingForSaleMaxPrice = ticker.Price;
                        _lastBuyTicker = ticker;
                        _readyForBuy = false;
                    }
                    else if (!_readyForBuy && _lastBuyTicker != null &&
                        (
                        (_waitingForSaleMaxPrice < _lastBuyTicker.Price) ||
                        (_waitingForSaleMaxPrice > _lastBuyTicker.Price && ticker.Price < _waitingForSaleMaxPrice - ((_waitingForSaleMaxPrice - _lastBuyTicker.Price) * (1 - _sellWalkbackPercentThreshold)))
                        ))
                    {
                        // SELL
                        TotalProfit += ticker.Price - _lastBuyTicker.Price;
                        LogFat("Sell at {@Price} Profit = {@Profit} Total = {@Total} Buy was {@Buy} Max was {$Max}", ticker.Price, ticker.Price - _lastBuyTicker.Price, TotalProfit, _lastBuyTicker.Price, _waitingForSaleMaxPrice);
                        _lastSellTicker = ticker;
                        _readyForBuy = true;
                        Sells++;
                        TotalHoldSecs += (ticker.Time - _lastBuyTicker.Time).TotalSeconds;
                    }
                    else if (_readyForBuy)
                    {
                        // WAIT
                        LogInfo($"WAITING TO BUY");
                    }
                    else
                    {
                        if (ticker.Price > _waitingForSaleMaxPrice) _waitingForSaleMaxPrice = ticker.Price;
                        // HODL
                        LogInfo($"HODL at {_lastBuyTicker.Price}");
                    }
                    _lastCheckTicker = ticker;
                }
            }
        }
    }
}
