﻿using CoinbasePro;
using CoinbasePro.Network.Authentication;
using CoinbasePro.Shared.Types;
using CoinbasePro.WebSocket.Models.Response;
using CoinbasePro.WebSocket.Types;
using Serilog;
using StrategyTrader.Strategies;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace StrategyTrader
{
    class Program
    {
        static string _outputDir = "C:/Code/me/StrategyAnalyzer/output/";
        static List<StrategyBase> _strategies = new List<StrategyBase>();
        static void Main(string[] args)
        {
            try
            {
                if (!Directory.Exists(_outputDir)) Directory.CreateDirectory(_outputDir);
                //configure the application logging to output to console and a file called log.txt
                Log.Logger = new LoggerConfiguration()
                                .MinimumLevel.Debug()
                                .WriteTo.Console()
                                .WriteTo.File($"{_outputDir}StrategyTrader-log.txt",
                                    rollingInterval: RollingInterval.Hour,
                                    rollOnFileSizeLimit: true)
                                .CreateLogger();

                // sandbox
                //var authenticator = new Authenticator("27a3b8ba6aaa0e3dfca4a99fab15cd84", "H0a8XUknjIGVl4qa/NwQYB42x/16ICzGEGHYi0xJdbKAugGyE7SfFTCq+QTnZlXcY5JPuQO740GUqwCV8HNWNg==", "2wqkx1cnwjh");
                //var client = new CoinbaseProClient(authenticator, true);

                // live no trade
                var authenticator = new Authenticator("5acf09eba10695ac424effec7734a9cf", "a5XDonChiTVLt2NZBaMD44DCRZW1wFgjNdOj+rhHtVQrQ8bQ8Teh50lPSqkw4PHsn7OrjQ6n4lCNutoFcUCGMQ==", "32mekher0iq");
                var client = new CoinbaseProClient(authenticator);

                // strategies
                for (int secs = 5; secs <= 120; secs += 5)
                    for (decimal buyThresh = 0.1m; buyThresh < 2; buyThresh += 0.1m)
                        for (decimal sellWBPercent = 0.95m; sellWBPercent > 0.5m; sellWBPercent -= 0.05m)
                            _strategies.Add(new Strategy1(secs, buyThresh, sellWBPercent));

                var productTypes = new List<ProductType>() { ProductType.BtcUsd };
                var channels = new List<ChannelType>() { ChannelType.Ticker };
                var webSocket = client.WebSocket;
                webSocket.OnTickerReceived += WebSocket_OnTickerReceived;
                webSocket.Start(productTypes, channels);

                while (true)
                    try
                    {
                        Thread.Sleep(10);
                    }
                    catch (Exception ex)
                    {
                        Log.Fatal("Exception: {@ex}", ex.Message);
                    }
            } 
            catch (Exception ex2) {
                Log.Fatal("Exception: {@ex}", ex2.Message);
            }
            Console.ReadLine();
        }

        static DateTime _lastPrint = DateTime.Now;
        static Ticker _firstTicker;
        private static void WebSocket_OnTickerReceived(object sender, WebfeedEventArgs<Ticker> e)
        {
            if (_firstTicker == null) _firstTicker = e.LastOrder;
            foreach (var strategy in _strategies) strategy.ExecuteStrategy(e.LastOrder);
            if ((e.LastOrder.Time - _lastPrint).TotalSeconds > 30)
            {
                foreach (var strategy in _strategies.Where(s => s.TotalProfit > 0).OrderBy(s => s.TotalProfit).ThenByDescending(s => s.Sells))
                    Log.Information($"{strategy.Name}: {{@TotalProfit}} ({{@Sells}} sells) (Hold Secs Avg: {{@HS}}) (Wait Secs Avg: {{@WS}})", strategy.TotalProfit, strategy.Sells, (strategy.TotalHoldSecs / strategy.Sells).ToString("F"), (strategy.TotalWaitSecs / strategy.Sells).ToString("F"));
                _lastPrint = DateTime.Now;
                Log.Fatal("WINNERS! (HODL: {@Hodl} ({@Mins} mins)", e.LastOrder.Price - _firstTicker.Price, (_firstTicker.Time - e.LastOrder.Time).TotalMinutes);
            }
        }
    }
}
