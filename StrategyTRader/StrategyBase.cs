﻿using CoinbasePro.WebSocket.Models.Response;
using Serilog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StrategyTrader
{
    public abstract class StrategyBase
    {
        private bool _Log = false;
        public int Sells { get; set; }
        public string Name { get; set; }
        public decimal TotalProfit { get; set; }
        public Ticker FirstTicker { get; set; }
        public double TotalHoldSecs { get; set; }
        public double TotalWaitSecs { get; set; }
        public abstract void ExecuteStrategy(Ticker ticker);
        public void LogInfo(string message, params object[] prams) { if (_Log) Log.Information($"[{Name}]: {message}", prams); }
        public void LogWarn(string message, params object[] prams) { if (_Log) Log.Warning($"[{Name}]: {message}", prams); }
        public void LogFat(string message, params object[] prams) { if (_Log) Log.Fatal($"[{Name}]: {message}", prams); }
    }
}
