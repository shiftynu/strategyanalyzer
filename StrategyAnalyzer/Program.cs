﻿using CoinbasePro;
using CoinbasePro.Network.Authentication;
using CoinbasePro.Services.Products.Models;
using CoinbasePro.Services.Products.Types;
using CoinbasePro.Shared.Types;
using Serilog;
using StrategyAnalyzer.Strategies;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace StrategyAnalyzer
{
    public class Program
    {
        static int _parallelizationCount = 6;
        static DateTime _downloadStartDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
        static DateTime _downloadEndDate = DateTime.Now;
        static string _downloadDataDir = "C:/Google Drive/CryptoData/";
        static CandleGranularity _downloadGranularity = CandleGranularity.Minutes1;
        static string _loadDataDir = "C:/Google Drive/CryptoData/Minutes1";
        static string _loadDataCoin = "Btc";
        static string _loadDataCurrency = "Usd";
        static string _outputDir = "C:/Code/me/StrategyAnalyzer/output/";
        static int _minPeriodSells = 4;

        //static string _runName = "TEST";
        //static string _runName = "1-7-30-60-90-days-StaticPairs";
        //static string _runName = "1-7-30-60-90-180-days-UpDownSameBS";
        static string _runName = "360-days-UpDownDiffMinPrice";

        private static List<StrategyPeriod> CreateStrategyPeriods()
        {
            var now = DateTime.Now;
            var strategyPeriods = new List<StrategyPeriod>();
            //strategyPeriods.Add(new StrategyPeriod("Last 1 Days", now.AddDays(-1), now));
            //strategyPeriods.Add(new StrategyPeriod("Last 2 Days", now.AddDays(-2), now));
            //strategyPeriods.Add(new StrategyPeriod("Last 7 Days", now.AddDays(-7), now));
            //strategyPeriods.Add(new StrategyPeriod("Last 30 Days", now.AddDays(-30), now));
            //strategyPeriods.Add(new StrategyPeriod("Last 60 Days", now.AddDays(-60), now));
            //strategyPeriods.Add(new StrategyPeriod("Last 90 Days", now.AddDays(-90), now));
            //strategyPeriods.Add(new StrategyPeriod("Last 180 Days", now.AddDays(-180), now));
            strategyPeriods.Add(new StrategyPeriod("Last 360 Days", now.AddDays(-360), now));
            return strategyPeriods;
        }

        private static List<StrategyBase> BuildStrategies(string productTypeString, List<StrategyPeriod> strategyPeriods, List<Candle> data)
        {
            var allDataStats = new DataStats(data);

            var strategies = new List<StrategyBase>();

            // static pair strategy
            //strategies.AddRange(StaticPair.BuildStrategies(productTypeString, strategyPeriods, allDataStats));
            //strategies.AddRange(UpDownSameBuySell.BuildStrategies(productTypeString, strategyPeriods, allDataStats));
            //strategies.AddRange(UpDownDiffBuySell.BuildStrategies(productTypeString, strategyPeriods, allDataStats));
            strategies.AddRange(UpDownDiffMinPrice.BuildStrategies(productTypeString, strategyPeriods, allDataStats));
            //strategies.AddRange(UpDownNoPriceBuySell.BuildStrategies(productTypeString, strategyPeriods, allDataStats));

            return strategies;
        }

        static async Task Main(string[] args)
        {

            //configure the application logging to output to console and a file called log.txt
            Log.Logger = new LoggerConfiguration()
                            .MinimumLevel.Debug()
                            .WriteTo.Console()
                            .WriteTo.File($"{_outputDir}StrategyAnalyzer-log.txt",
                                rollingInterval: RollingInterval.Hour,
                                rollOnFileSizeLimit: true)
                            .CreateLogger();
            try
            {
                var authenticator = new Authenticator("3bbaba994c648dba580588fcd1504c3f", "yZHIMdFcJ8zg72DJkI2EtJRRdECvZrghyJW3AF8QxMLmdQF/5zui43/cb+nlZKN+mm6aeqiS32+nd+xuy6lcYQ==", "5D014ouU7#q#");
                var client = new CoinbaseProClient(authenticator);

                var response = "";
                while (true)
                {
                    var treeResponse = "";
                    Console.WriteLine("(d)ownload or (a)nalyze or (o)netime runner or (e)xit?");
                    response = Console.ReadLine();
                    if (response == "e") break;
                    else treeResponse = response;

                    if (treeResponse == "o")
                    {
                        var d = DateTime.Parse("06/15/20 23:12");
                        Console.WriteLine(d);
                        d = DateTime.Parse("06/15/20 08:12");
                        Console.WriteLine(d);
                    }
                    else if (treeResponse == "d")
                    {
                        Console.WriteLine($"Start Date (default: {_downloadStartDate.ToString("MM/dd/yy")}):");
                        response = Console.ReadLine();
                        if (response != "") _downloadStartDate = DateTime.Parse(response);

                        Console.WriteLine($"End Date (default: {DateTime.Now.ToString("MM/dd/yy")}):");
                        response = Console.ReadLine();
                        if (response != "") _downloadEndDate = DateTime.Parse(response);

                        Console.WriteLine($"Download Data Dir (default: {_downloadDataDir}):");
                        response = Console.ReadLine();
                        if (response != "") _downloadDataDir = response;
                        if (!Directory.Exists(_downloadDataDir)) Directory.CreateDirectory(_downloadDataDir);
                        await DownloadHistoricalData(client, _downloadDataDir);
                    }
                    else
                    {
                        Console.WriteLine($"Coin (default: {_loadDataCoin}):");
                        response = Console.ReadLine();
                        if (response != "") _loadDataCoin = response;

                        Console.WriteLine($"Currency (default: {_loadDataCurrency}):");
                        response = Console.ReadLine();
                        if (response != "") _loadDataCurrency = response;

                        //Console.WriteLine($"Load Data Dir (default: {_loadDataDir}):");
                        //response = Console.ReadLine();
                        //if (response != "") _loadDataDir = response;

                        //Console.WriteLine($"Output Dir (default: {_outputDir}):");
                        //response = Console.ReadLine();
                        //if (response != "") _outputDir = response;

                        if (!Directory.Exists(_outputDir)) Directory.CreateDirectory(_outputDir);

                        // TODO only load the dates covered by the periods
                        var strategyPeriods = CreateStrategyPeriods();
                        var loadStartDate = strategyPeriods.Min(p => p.PeriodStartDate);
                        var loadEndDate = strategyPeriods.Min(p => p.PeriodEndDate);
                        var allData = LoadData(loadStartDate, loadEndDate);

                        foreach (var productTypeString in allData.Keys)
                        {
                            Console.WriteLine($"Starting {productTypeString}...");
                            strategyPeriods.ForEach(p => p.InitStats(allData[productTypeString]));
                            foreach (var period in strategyPeriods)
                                Console.WriteLine($"Period: {period}");
                            var strategies = BuildStrategies(productTypeString, strategyPeriods, allData[productTypeString]);
                            var results = await RunStrategies(productTypeString, strategies, allData[productTypeString]);
                            SaveResults(productTypeString, results, _runName);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
                Console.ReadLine();
            }
        }

        private static void SaveResults(string productTypeString, List<StrategyResult> results, string runName)
        {
            if (results.Count == 0) return;
            var fctr = 0;
            var fileName = $"{_outputDir}{productTypeString}-{runName}-{fctr++}";
            while (File.Exists(fileName + ".csv"))
                fileName = $"{_outputDir}{productTypeString}-{runName}-{fctr++}";

            // serialize all results
            //Console.WriteLine($"Saving {fileName}.bin...");
            //IFormatter formatter = new BinaryFormatter();
            //Stream stream = new FileStream(fileName + ".bin", FileMode.Create, FileAccess.Write, FileShare.None);
            //formatter.Serialize(stream, results);
            //stream.Close();

            // sort and pick results
            var trimmedResults = results.Where(r => r != null && r.MaxPeriodSells > _minPeriodSells && (results[0].PeriodResults.Count == 1 || r.AvgPeriodGain != r.BestPeriodGain)).OrderByDescending(r => r.Score).ToList();

            if (trimmedResults.Count == 0)
                trimmedResults = results.Where(r => r != null).OrderByDescending(r => r.Score).ToList();

            var csv = new StringBuilder();
            var periodCount = trimmedResults[0].PeriodResults.Count;

            // score combinations
            var scoreCombs = GetCombinations(results[0].PeriodResults.Select(p => (int)p.PeriodDaysCount).ToList()).OrderBy(s => s).ToList();
            scoreCombs = scoreCombs.Where(s => s.Count(c => c == '|') > 1).ToList();
                       
            // header
            var headerLine = $"Strategy Name,Best PTG,Avg PTG,Best PDG,Avg PDG,PWS,Score";
            foreach (var scoreComb in scoreCombs) headerLine += $",Score {scoreComb}";
            for (int i = 0; i < periodCount; i++)
            {
                var p = $"P{i}:";
                headerLine += $",{p}Desc,{p}HODL,{p}TP,{p}PPS,{p}Gain,{p}Gain Comp,{p}Avg Day Gain,{p}Avg Day Gain Comp,{p}Avg Hold Days,{p}Avg Wait Days,{p}Sells";
                //headerLine += $",{p}Trades";
            }
            csv.AppendLine(headerLine);

            // results
            foreach (var result in trimmedResults)
            {
                var resultLine = $"{result.Name.Replace(",",";")},{result.BestPeriodGain.ToString("F")},{result.AvgPeriodGain.ToString("F")},{result.BestPeriodDayGain.ToString("F")},{result.AvgPeriodDayGain.ToString("F")},{result.PeriodsWithSellsCount} of {result.PeriodResults.Count},{result.Score.ToString("F")}";
                foreach (var scoreComb in scoreCombs)
                {
                    var scorePeriods = result.PeriodResults.Where(p => scoreComb.Contains($"{p.PeriodDaysCount}|"));
                    var periodsWithSellsCount = scorePeriods.Count(p => p.SellCount > 0);
                    var avgPeriodDayGain = scorePeriods.Sum(p => p.DailyAveragePeriodGain) / scorePeriods.Count();
                    var score = periodsWithSellsCount * avgPeriodDayGain;
                    resultLine += "," + score.ToString("F");
                }
                for (int i = 0; i < periodCount; i++)
                {
                    resultLine += $",{result.PeriodResults[i].Name},{((result.PeriodResults[i].PeriodClose - result.PeriodResults[i].PeriodOpen) * 100 / result.PeriodResults[i].PeriodOpen).ToString("F")},{result.PeriodResults[i].PeriodProfit.ToString("F")},{result.PeriodResults[i].PeriodProfitPerSell.ToString("F")},{result.PeriodResults[i].PeriodGain.ToString("F")},{result.PeriodResults[i].PeriodGainCompounding.ToString("F")},{result.PeriodResults[i].DailyAveragePeriodGain.ToString("F")},{result.PeriodResults[i].DailyAveragePeriodGainCompounding.ToString("F")},{result.PeriodResults[i].AverageHoldDays.ToString("F")},{result.PeriodResults[i].AverageWaitDays.ToString("F")},{result.PeriodResults[i].SellCount}";
                    //resultLine += $",{result.PeriodResults[i].TradeListString}";
                }
                csv.AppendLine(resultLine);
            }
            Console.WriteLine($"Saving {fileName}.csv...");
            File.WriteAllText(fileName + ".csv", csv.ToString());
        }

        private static List<string> GetCombinations(List<int> list)
        {
            var combos = new List<string>();
            double count = Math.Pow(2, list.Count);
            for (int i = 1; i <= count - 1; i++)
            {
                var combo = "";
                string str = Convert.ToString(i, 2).PadLeft(list.Count, '0');
                for (int j = 0; j < str.Length; j++)
                    if (str[j] == '1')
                        combo += list[j] + "|";
                combos.Add(combo);
            }
            return combos;
        }

        private static async Task<List<StrategyResult>> RunStrategies(string productTypeString, List<StrategyBase> strategies, List<Candle> data)
        {
            var results = new List<StrategyResult>();

            Console.WriteLine($"Running {productTypeString} - Num Strategies: {strategies.Count}");
            var ctr = 0;
            var startedAt = DateTime.Now;

            var allTasks = new List<Task>();
            var count = strategies.Count / _parallelizationCount;
            for (int i = 0; i < _parallelizationCount; i++)
            {
                var offset = count * i;

                var paraChunk = strategies.Skip(offset).Take(count);

                allTasks.Add(Task.Run(() =>
                {
                    foreach (var strategy in paraChunk) strategy.Initialize();
                    for (int candleIdx = 0; candleIdx < data.Count; candleIdx++)
                    {
                        var candle = data[candleIdx];
                        if (ctr > 0 && ctr % (data.Count / (100 / _parallelizationCount)) == 0)
                        {
                            var elapsedSecs = (DateTime.Now - startedAt).TotalSeconds;
                            var percentDone = (((double)(ctr / _parallelizationCount) / (double)data.Count) * 100.0);
                            var estRem = (100 - percentDone) * (elapsedSecs / percentDone);
                            Console.WriteLine($"{productTypeString}: {percentDone.ToString("F")}%\tSecs: {elapsedSecs.ToString("F")}\tEst secs left: {estRem.ToString("F")}\tEst total secs: {(elapsedSecs + estRem).ToString("F")}");
                        }
                        ctr++;
                        foreach (var strategy in paraChunk) strategy.Update(candleIdx, data, ctr);
                    }
                    foreach (var strategy in paraChunk) results.Add(strategy.Finish());
                }));
            }

            await Task.WhenAll(allTasks.ToArray());

            return results;
        }

        private static Dictionary<string, List<Candle>> LoadData(DateTime startDate, DateTime endDate)
        {
            var data = new Dictionary<string, List<Candle>>();

            var currentMonth = startDate.Month;
            var currentYear = startDate.Year;
            var filePrefix = $"{_loadDataCoin}{_loadDataCurrency}";
            while (currentYear < endDate.Year || (currentYear == endDate.Year && currentMonth <= endDate.Month))
            {
                var fileName = $"{_loadDataDir}{_loadDataCurrency}/{filePrefix}-{currentYear}-{currentMonth}.bin";
                if (File.Exists(fileName))
                {
                    Console.WriteLine($"Loading {filePrefix} - {currentMonth}/{currentYear}");
                    IFormatter formatter = new BinaryFormatter();
                    Stream stream = new FileStream(fileName, FileMode.Open, FileAccess.Read, FileShare.Read);
                    var obj = (List<Candle>)formatter.Deserialize(stream);
                    stream.Close();
                    if (!data.ContainsKey(filePrefix))
                        data[filePrefix] = obj;
                    else
                        data[filePrefix].AddRange(obj);
                }
                currentMonth++;
                if (currentMonth == 13)
                {
                    currentMonth = 1;
                    currentYear++;
                }
            }
            // trim all data to start end end dates
            if (data.ContainsKey(filePrefix))
                data[filePrefix] = data[filePrefix].Where(d => d.Time >= startDate && d.Time <= endDate).ToList();

            return data;
        }

        private static async Task DownloadHistoricalData(CoinbaseProClient client, string dataDir)
        {
            var ddir = $"{dataDir}/{_downloadGranularity.ToString()}/";
            if (!Directory.Exists(ddir)) Directory.CreateDirectory(ddir);
            foreach (ProductType productType in Enum.GetValues(typeof(ProductType)))
            {
                var currentMonth = _downloadStartDate.Month;
                var currentYear = _downloadStartDate.Year;
                while (currentYear < _downloadEndDate.Year || (currentYear == _downloadEndDate.Year && currentMonth <= _downloadEndDate.Month))
                {
                    var fileName = $"{ddir}{productType.ToString()}-{currentYear}-{currentMonth}.bin";
                    if (!File.Exists(fileName))
                    {
                        Console.WriteLine($"Downloading {productType.ToString()} - {currentMonth}/{currentYear}");
                        var start = new DateTime(currentYear, currentMonth, 1);
                        var end = start.AddMonths(1).AddSeconds(-1);
                        if (end > DateTime.Now) end = DateTime.Now;
                        var historicalData = await client.ProductsService.GetHistoricRatesAsync(productType, start, end, _downloadGranularity);
                        historicalData = historicalData.Reverse().ToList();
                        if (historicalData.Count > 0)
                        {
                            Console.WriteLine($"Saving {productType.ToString()} - {currentMonth}/{currentYear}");
                            IFormatter formatter = new BinaryFormatter();
                            Stream stream = new FileStream(fileName, FileMode.Create, FileAccess.Write, FileShare.None);
                            formatter.Serialize(stream, historicalData);
                            stream.Close();
                        }
                        else
                        {
                            Console.WriteLine($"No data     {productType.ToString()} - {currentMonth}/{currentYear}");
                        }
                        Thread.Sleep(1000);
                    }
                    else
                    {
                        Console.WriteLine($"Skipping {productType.ToString()} - {currentMonth}/{currentYear}");
                    }
                    currentMonth++;
                    if (currentMonth == 13)
                    {
                        currentMonth = 1;
                        currentYear++;
                    }
                }
            }
        }
    }

}

