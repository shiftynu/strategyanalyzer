﻿using CoinbasePro.Services.Products.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StrategyAnalyzer
{
    public class DataStats
    {
        public DataStats(List<Candle> data)
        {
            if (data.Count == 0) return;
            FirstCandle = data.First();
            LastCandle = data.Last();
            StartDate = FirstCandle.Time;
            EndDate = LastCandle.Time;
            var maxPrice = 0.0;
            var minPrice = 10000000.0;
            var totalPrice = 0.0;
            foreach (var candle in data)
            {
                if ((double)candle.Open < minPrice) minPrice = (double)candle.Open;
                if ((double)candle.Open > maxPrice) maxPrice = (double)candle.Open;
                totalPrice += (double)candle.Open;
            }
            MinPrice = minPrice;
            MaxPrice = maxPrice;
            AvgPrice = totalPrice / data.Count;
            PeriodOpen = (double)data.First().Open;
            PeriodClose = (double)data.Last().Open;
        }

        public double PeriodOpen { get; set; }
        public double PeriodClose { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public Candle FirstCandle { get; set; }
        public Candle LastCandle { get; set; }
        public double MinPrice { get; set; }
        public double MaxPrice { get; set; }
        public double AvgPrice { get; set; }
    }
}
