﻿using CsvHelper.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StrategyAnalyzer
{
    [Serializable]
    public class StrategyResult
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public List<StrategyPeriodResult> PeriodResults { get; set; }
        public double BestPeriodGain { get { return PeriodResults.Max(p => p.PeriodGain); } }
        public double AvgPeriodGain { get { return PeriodResults.Sum(p => p.PeriodGain) / PeriodResults.Count; } }
        public double BestPeriodDayGain { get { return PeriodResults.Max(p => p.DailyAveragePeriodGain); } }
        public double AvgPeriodDayGain { get { return PeriodResults.Sum(p => p.DailyAveragePeriodGain) / PeriodResults.Count; } }
        public int PeriodsWithSellsCount { get { return PeriodResults.Where(p => p.SellCount > 0).Count();  } }
        public double Score
        {
            get
            {
                return PeriodsWithSellsCount * AvgPeriodDayGain;

            }
        }

        public int MaxPeriodSells { 
            get
            {
                return PeriodResults.Max(p => p.SellCount);
            }
        }
    }
}
