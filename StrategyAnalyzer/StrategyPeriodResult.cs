﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StrategyAnalyzer
{
    [Serializable]
    public class StrategyPeriodResult
    {
        public string Name { get; set; }
        public DateTime FirstBuyDate { get; set; }
        public DateTime LastSellDate { get; set; }
        public DateTime PeriodStartDate { get; set; }
        public DateTime PeriodEndDate { get; set; }
        public double PeriodDaysCount { get { return Math.Ceiling((PeriodEndDate - PeriodStartDate).TotalDays); } }
        public double PeriodGain { get; set; }
        public double PeriodGainCompounding { get; set; }
        public double DailyAveragePeriodGain { get { return PeriodGain / PeriodDaysCount; } }
        public double DailyAveragePeriodGainCompounding { get { return PeriodGainCompounding / PeriodDaysCount; } }
        public List<Trade> Trades { get; set; }
        public string TradeListString
        {
            get
            {
                return string.Join("|", Trades);
            }
        }
        public double AverageHoldDays { get; set; }
        public double AverageWaitDays { get; set; }
        public int SellCount { get; set; }

        public double PeriodOpen { get; set; }
        public double PeriodClose { get; set; }
        public double PeriodProfitPerSell { get; internal set; }
        public double PeriodProfit { get; internal set; }
    }
}
