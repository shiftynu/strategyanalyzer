﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StrategyAnalyzer
{
    [Serializable]
    public class Trade
    {
        public string Type { get; set; }
        public string Timestamp { get; set; }
        public double Price { get; set; }

        public override string ToString()
        {
            return $"{Type}:{Price}({Timestamp})";
        }
    }
}
