﻿using CoinbasePro.Services.Products.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StrategyAnalyzer.Strategies
{
    public class PeriodData
    {
        public double ReinvestValue { get; set; }
        public double WithdrawProfit { get; set; }
        public DateTime FirstBuyDate { get; set; }
        public Candle LastBuy { get; set; }
        public Candle LastSell { get; set; }
        public int BuyCount { get; set; }
        public int SellCount { get; set; }
        public TimeSpan HoldTime { get; set; }
        public TimeSpan WaitTime { get; set; }
        public List<Trade> Trades { get; set; }
        public string TradeListString
        {
            get
            {
                return string.Join(",", Trades);
            }
        }

        public double TotalProfit { get; internal set; }
    }
}
