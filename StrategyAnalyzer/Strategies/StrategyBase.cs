﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CoinbasePro.Services.Products.Models;

namespace StrategyAnalyzer.Strategies
{
    public abstract class StrategyBase
    {
        static bool _saveTradeList = false;

        public StrategyBase(string strategyName, List<StrategyPeriod> periods)
        {
            StrategyName = strategyName;
            Periods = periods;
        }
        public List<StrategyPeriod> Periods { get; set; }
        public string StrategyName { get; set; }
        public void Initialize()
        {
            foreach (var period in Periods)
                PeriodInitialize(period);
        }
        internal abstract void PeriodInitialize(StrategyPeriod period);
        public void Update(int candleIdx, List<Candle> data, int ctr)
        {
            foreach (var period in Periods)
                if (data[candleIdx].Time >= period.Stats.StartDate && data[candleIdx].Time <= period.Stats.EndDate)
                    PeriodUpdate(period, candleIdx, data, ctr);
        }
        internal abstract void PeriodUpdate(StrategyPeriod period, int candleIdx, List<Candle> data, int ctr);
        public StrategyResult Finish()
        {
            var strategyResult = new StrategyResult { Name = StrategyName, Description = "TODO", PeriodResults = new List<StrategyPeriodResult>() };
            foreach (var period in Periods)
                strategyResult.PeriodResults.Add(PeriodFinish(period));
            return strategyResult;
        }
        internal abstract StrategyPeriodResult PeriodFinish(StrategyPeriod period);
        internal void PeriodBuy(PeriodData periodData, Candle candle)
        {
            periodData.BuyCount++;
            periodData.LastBuy = candle;
            if (_saveTradeList) periodData.Trades.Add(new Trade { Timestamp = candle.Time.ToString("MM/dd/yy HH:mm"), Type = "BUY", Price = (double)candle.Open });
            if (periodData.LastSell != null) periodData.WaitTime = periodData.WaitTime.Add(candle.Time - periodData.LastSell.Time);
        }
        internal void PeriodSell(PeriodData periodData, Candle candle)
        {
            var profitPercent = (candle.Open - periodData.LastBuy.Open) / periodData.LastBuy.Open;
            periodData.TotalProfit += (double)(candle.Open - periodData.LastBuy.Open);
            periodData.WithdrawProfit += (double)profitPercent;
            periodData.ReinvestValue += (periodData.ReinvestValue * (double)profitPercent);
            periodData.SellCount++;
            periodData.LastSell = candle;
            if (_saveTradeList) periodData.Trades.Add(new Trade { Timestamp = candle.Time.ToString("MM/dd/yy HH:mm"), Type = "SELL", Price = (double)candle.Open });
            if (periodData.LastBuy != null) periodData.HoldTime = periodData.HoldTime.Add(candle.Time - periodData.LastBuy.Time);
        }
    }
}
