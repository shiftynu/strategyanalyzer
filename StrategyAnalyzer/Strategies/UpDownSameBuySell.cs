﻿using System;
using System.Collections.Generic;
using CoinbasePro.Services.Products.Models;

namespace StrategyAnalyzer.Strategies
{
    class UpDownPeriodData : PeriodData
    {
        public int TimeGranularity { get; set; }
        public double ChangePercentTrigger { get; set; }
        public double ChangeAmountTrigger { get; set; }
        public bool WaitingForDown { get; set; }
        public int IgnoreToIndex { get; internal set; }
        public List<Candle> CandleHistory { get; set; }
    }

    public class UpDownSameBuySell : StrategyBase
    {
        private Dictionary<string, UpDownPeriodData> _periodData = new Dictionary<string, UpDownPeriodData>();

        public UpDownSameBuySell(string strategyName, List<StrategyPeriod> periods) : base(strategyName, periods) { }

        public static List<StrategyBase> BuildStrategies(string productTypeString, List<StrategyPeriod> periods, DataStats allDataStats)
        {
            var strategies = new List<StrategyBase>();
            var timeGranularities = new List<int>();
            //for (int i = 1; i < 30; i+=3) timeGranularities.Add(i);
            for (int i = 1; i < 2880; i++) timeGranularities.Add(i);
            for (int i = 2910; i <= 40320; i += 30) timeGranularities.Add(i);
            var changeAmountTriggers = new List<double>() { 0.5, 1, 1.5, 2, 2.5, 3, 4, 5, 7, 9, 15, 20, 30, 50, 75, 100, 150, 200, 300, 500 };
            var changePercentTriggers = new List<double>() { 0.0001, 0.0003, 0.0005, 0.00075, 0.001, 0.002, 0.005, 0.0075, 0.01, 0.013, 0.015, 0.017, 0.019, 0.02, 0.021, 0.023, 0.025, 0.027, 0.03, 0.035, 0.04, 0.05, 0.6, 0.07, 0.08, 0.09, 0.1, 0.12, 0.15, 0.2 };

            // build amount triggers to check
            foreach (var timeGranularity in timeGranularities)
            {
                foreach (var changeAmountTrigger in changeAmountTriggers)
                {
                    var strategy = new UpDownSameBuySell($"UpDownSameBSAmount ({timeGranularity} - {(changeAmountTrigger).ToString("F")})", periods);
                    foreach (var period in periods)
                    {
                        var periodData = new UpDownPeriodData
                        {
                            TimeGranularity = timeGranularity,
                            ChangeAmountTrigger = changeAmountTrigger,
                            HoldTime = new TimeSpan(),
                            WaitTime = new TimeSpan(),
                            ReinvestValue = 1,
                            WithdrawProfit = 0,
                            WaitingForDown = true,
                            Trades = new List<Trade>()
                        };
                        strategy._periodData[period.Name] = periodData;
                    }
                    strategies.Add(strategy);
                }

                foreach (var changePercentTrigger in changePercentTriggers)
                {
                    var strategy = new UpDownSameBuySell($"UpDownSameBS% ({timeGranularity} - {(changePercentTrigger * 100).ToString("F")})", periods);
                    foreach (var period in periods)
                    {
                        var periodData = new UpDownPeriodData
                        {
                            TimeGranularity = timeGranularity,
                            ChangePercentTrigger = changePercentTrigger,
                            HoldTime = new TimeSpan(),
                            WaitTime = new TimeSpan(),
                            ReinvestValue = 1,
                            WithdrawProfit = 0,
                            WaitingForDown = true,
                            Trades = new List<Trade>()
                        };
                        strategy._periodData[period.Name] = periodData;
                    }
                    strategies.Add(strategy);
                }
            }

            return strategies;
        }


        internal override void PeriodInitialize(StrategyPeriod period)
        {
            
        }

        internal override void PeriodUpdate(StrategyPeriod period, int candleIdx, List<Candle> data, int ctr)
        {
            var candle = data[candleIdx];
            var periodData = _periodData[period.Name];

            // check time granularity passed
            if (candleIdx < periodData.IgnoreToIndex) return;

            // wait till we hit the first buy point
            if (periodData.LastBuy == null)
            {
                PeriodBuy(periodData, candle);
                periodData.IgnoreToIndex = candleIdx + periodData.TimeGranularity;
                periodData.FirstBuyDate = candle.Time;
            }
            else
            {
                var lastCompareCandle = data[candleIdx - periodData.TimeGranularity];

                if (periodData.WaitingForDown)
                {
                    if (periodData.ChangeAmountTrigger != 0)
                    {
                        // change by amount
                        // time to sell
                        if ((double)lastCompareCandle.Open - (double)candle.Open > periodData.ChangeAmountTrigger)
                        {
                            PeriodSell(periodData, candle);
                            periodData.WaitingForDown = false;
                        }
                    } 
                    else
                    {
                        // change by percent
                        // time to sell
                        if (((double)lastCompareCandle.Open - (double)candle.Open)/(double)lastCompareCandle.Open > periodData.ChangePercentTrigger)
                        {
                            PeriodSell(periodData, candle);
                            periodData.WaitingForDown = false;
                        }
                    }
                }
                else
                {
                    if (periodData.ChangeAmountTrigger != 0)
                    {
                        // change by amount
                        // time to buy
                        if ((double)candle.Open - (double)lastCompareCandle.Open > periodData.ChangeAmountTrigger)
                        {
                            PeriodBuy(periodData, candle);
                            periodData.WaitingForDown = true;
                        }
                    }
                    else
                    {
                        // change by percent
                        // time to buy
                        if (((double)candle.Open - (double)lastCompareCandle.Open)/(double)lastCompareCandle.Open > periodData.ChangePercentTrigger)
                        {
                            PeriodBuy(periodData, candle);
                            periodData.WaitingForDown = true;
                        }
                    }
                }
            }
        }
        internal override StrategyPeriodResult PeriodFinish(StrategyPeriod period)
        {
            var periodResult = new StrategyPeriodResult();
            var periodData = _periodData[period.Name];
            periodResult.PeriodProfit = periodData.TotalProfit;
            periodResult.PeriodProfitPerSell = periodData.TotalProfit / periodData.SellCount;
            periodResult.PeriodGain = periodData.WithdrawProfit * 100;
            periodResult.PeriodGainCompounding = (periodData.ReinvestValue - 1) * 100;
            periodResult.FirstBuyDate = periodData.FirstBuyDate;
            periodResult.LastSellDate = periodData.LastSell != null ? periodData.LastSell.Time : default(DateTime);
            periodResult.PeriodStartDate = period.PeriodStartDate;
            periodResult.PeriodEndDate = period.PeriodEndDate;
            periodResult.PeriodOpen = period.Stats.PeriodOpen;
            periodResult.PeriodClose = period.Stats.PeriodClose;
            periodResult.Trades = periodData.Trades;
            periodResult.Name = period.Name;
            periodResult.SellCount = periodData.SellCount;
            periodResult.AverageHoldDays = periodData.HoldTime.TotalDays / (period.PeriodEndDate - period.PeriodStartDate).TotalDays;
            periodResult.AverageWaitDays = periodData.WaitTime.TotalDays / (period.PeriodEndDate - period.PeriodStartDate).TotalDays;
            return periodResult;
        }
    }
}
