﻿using System;
using System.Collections.Generic;
using System.Linq;
using CoinbasePro.Services.Products.Models;

namespace StrategyAnalyzer.Strategies
{
    class StaticPairPeriodData : PeriodData
    {
        public double Bottom { get; set; }
        public double Top { get; set; }
        public double Spread { get { return Top - Bottom; } }
        public bool NeedsSell { get; internal set; }
    }

    public class StaticPair : StrategyBase
    {
        static double _minSpreadPercent = 0.001;
        static double _incrementDivisor = 1000;
        static double _firstPurchaseTolerance = 0.001;

        static bool _saveTradeList = false;

        private Dictionary<string, StaticPairPeriodData> _periodData = new Dictionary<string, Strategies.StaticPairPeriodData>();

        public StaticPair(string strategyName, List<StrategyPeriod> periods) : base(strategyName, periods) { }

        public static List<StrategyBase> BuildStrategies(string productTypeString, List<StrategyPeriod> periods, DataStats allDataStats)
        {
            var strategies = new List<StrategyBase>();

            // choose increment for pairs
            var increment = Math.Max((allDataStats.MaxPrice - allDataStats.MinPrice) / _incrementDivisor, 1);
            var minSpread = (_minSpreadPercent * allDataStats.MaxPrice);

            // build pairs to check
            for (double bottom = allDataStats.MinPrice - 1; bottom < allDataStats.MaxPrice; bottom += increment)
            {
                for (double top = allDataStats.MaxPrice; top > bottom; top -= increment)
                {
                    if ((top - bottom) > minSpread)
                    {
                        var strategy = new StaticPair($"StaticPair ({bottom.ToString("F")} - {top.ToString("F")} - {(top - bottom).ToString("F")})", periods);
                        foreach (var period in periods)
                        {
                            var periodData = new StaticPairPeriodData
                            {
                                Bottom = bottom,
                                Top = top,
                                NeedsSell = true,
                                HoldTime = new TimeSpan(),
                                WaitTime = new TimeSpan(),
                                ReinvestValue = 1,
                                WithdrawProfit = 0,
                                Trades = new List<Trade>()
                            };
                            strategy._periodData[period.Name] = periodData;
                        }
                        strategies.Add(strategy);
                    }
                }
            }

            return strategies;
        }

        internal override void PeriodInitialize(StrategyPeriod period)
        {
            
        }

        internal override void PeriodUpdate(StrategyPeriod period, int candleIdx, List<Candle> data, int ctr)
        {
            var candle = data[candleIdx];
            var periodData = _periodData[period.Name];
            // wait till we hit the first buy point
            if (periodData.LastBuy == null)
            {
                if (Math.Abs((double)candle.Open - (double)periodData.Bottom) / (double)periodData.Bottom < _firstPurchaseTolerance)
                {
                    PeriodBuy(periodData, candle);
                    periodData.FirstBuyDate = candle.Time;
                }
            }
            else
            {
                if (periodData.NeedsSell)
                {
                    // time to sell
                    if ((double)candle.Open >= periodData.Top)
                    {
                        PeriodSell(periodData, candle);
                        periodData.NeedsSell = false;
                    }
                }
                else
                {
                    // time to buy
                    if ((double)candle.Open <= periodData.Bottom)
                    {
                        PeriodBuy(periodData, candle);
                        periodData.NeedsSell = true;
                    }
                }
            }
        }

        internal override StrategyPeriodResult PeriodFinish(StrategyPeriod period)
        {
            var periodResult = new StrategyPeriodResult();
            var periodData = _periodData[period.Name];
            periodResult.PeriodProfit = periodData.TotalProfit;
            periodResult.PeriodProfitPerSell = periodData.TotalProfit / periodData.SellCount;
            periodResult.PeriodGain = periodData.WithdrawProfit * 100;
            periodResult.PeriodGainCompounding = (periodData.ReinvestValue - 1) * 100;
            periodResult.FirstBuyDate = periodData.FirstBuyDate;
            periodResult.LastSellDate = periodData.LastSell != null ? periodData.LastSell.Time : default(DateTime);
            periodResult.PeriodStartDate = period.PeriodStartDate;
            periodResult.PeriodEndDate = period.PeriodEndDate;
            periodResult.PeriodOpen = period.Stats.PeriodOpen;
            periodResult.PeriodClose = period.Stats.PeriodClose;
            periodResult.Trades = periodData.Trades;
            periodResult.Name = period.Name;
            periodResult.SellCount = periodData.SellCount;
            periodResult.AverageHoldDays = periodData.HoldTime.TotalDays/(period.PeriodEndDate - period.PeriodStartDate).TotalDays;
            periodResult.AverageWaitDays = periodData.WaitTime.TotalDays / (period.PeriodEndDate - period.PeriodStartDate).TotalDays;
            return periodResult;
        }
    }
}
