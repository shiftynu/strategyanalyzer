﻿using System;
using System.Collections.Generic;
using CoinbasePro.Services.Products.Models;

namespace StrategyAnalyzer.Strategies
{
    public class UpDownDPeriodData : PeriodData
    {
        public int TimeGranularity { get; set; }
        public double ChangePercentTriggerBuy { get; set; }
        public double ChangePercentTriggerSell { get; set; }
        public double ChangeAmountTriggerBuy { get; set; }
        public double ChangeAmountTriggerSell { get; set; }
        public bool WaitingForDown { get; set; }
        public int IgnoreToIndex { get; internal set; }
    }

    public class UpDownDiffBuySell : StrategyBase
    {
        private Dictionary<string, UpDownDPeriodData> _periodData = new Dictionary<string, UpDownDPeriodData>();

        public UpDownDiffBuySell(string strategyName, List<StrategyPeriod> periods) : base(strategyName, periods) { }

        public static List<StrategyBase> BuildStrategies(string productTypeString, List<StrategyPeriod> periods, DataStats allDataStats)
        {
            var strategies = new List<StrategyBase>();
            var timeGranularities = new List<int>();
            //for (int i = 1; i < 30; i+=3) timeGranularities.Add(i);
            for (int i = 1; i < 2880; i++) timeGranularities.Add(i);
            for (int i = 2910; i <= 40320; i += 60) timeGranularities.Add(i);
            //var changeAmountTriggersBuy = new List<double>() { 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1, 1.25, 1.5, 2, 2.5, 3, 4, 5, 7, 9, 15, 20, 30, 50, 75, 100, 150, 200, 300, 500 };
            //var changeAmountTriggersSell = new List<double>() { 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1, 1.25, 1.5, 2, 2.5, 3, 4, 5, 7, 9, 15, 20, 30, 50, 75, 100, 150, 200, 300, 500 };
            //var changePercentTriggersBuy = new List<double>() { 0.0001, 0.0003, 0.0005, 0.00075, 0.001, 0.002, 0.005, 0.0075, 0.01, 0.013, 0.015, 0.017, 0.019, 0.02, 0.021, 0.023, 0.025, 0.027, 0.03, 0.035, 0.04, 0.05, 0.6, 0.07, 0.08, 0.09, 0.1, 0.12, 0.15, 0.2 };
            //var changePercentTriggersSell = new List<double>() { 0.0001, 0.0003, 0.0005, 0.00075, 0.001, 0.002, 0.005, 0.0075, 0.01, 0.013, 0.015, 0.017, 0.019, 0.02, 0.021, 0.023, 0.025, 0.027, 0.03, 0.035, 0.04, 0.05, 0.6, 0.07, 0.08, 0.09, 0.1, 0.12, 0.15, 0.2 };
            //var changeAmountTriggersBuy = new List<double>() { 0.5, 1, 1.5, 2, 3, 5, 9, 20, 50, 75, 200, 500 };
            //var changeAmountTriggersSell = new List<double>() { 0.5, 1, 1.5, 2, 3, 5, 9, 20, 50, 75, 200, 500 };
            //var changePercentTriggersBuy = new List<double>() { 0.00001, 0.0001, 0.001, 0.01 };
            //var changePercentTriggersSell = new List<double>() { 0.00001, 0.0001, 0.001, 0.01 };
            var changePercentTriggersBuy = new List<double>() { 0.001, 0.005, 0.01, 0.015, 0.02, 0.025, 0.03, 0.05 };
            var changePercentTriggersSell = new List<double>() { 0.001, 0.005, 0.01, 0.015, 0.02, 0.025, };

            // build amount triggers to check
            foreach (var timeGranularity in timeGranularities)
            {
                //foreach (var changeAmountTriggerBuy in changeAmountTriggersBuy)
                //{
                //    foreach (var changeAmountTriggerSell in changeAmountTriggersSell)
                //    {
                //        var strategy = new UpDownDiffBuySell($"UpDownDiffBSAmount ({timeGranularity} - {(changeAmountTriggerBuy).ToString("F")}|{(changeAmountTriggerSell).ToString("F")})", periods);
                //        foreach (var period in periods)
                //        {
                //            var periodData = new UpDownDPeriodData
                //            {
                //                TimeGranularity = timeGranularity,
                //                ChangeAmountTriggerBuy = changeAmountTriggerBuy,
                //                ChangeAmountTriggerSell = changeAmountTriggerSell,
                //                HoldTime = new TimeSpan(),
                //                WaitTime = new TimeSpan(),
                //                ReinvestValue = 1,
                //                WithdrawProfit = 0,
                //                WaitingForDown = true,
                //                Trades = new List<Trade>()
                //            };
                //            strategy._periodData[period.Name] = periodData;
                //        }
                //        strategies.Add(strategy);
                //    }
                //}

                foreach (var changePercentTriggerBuy in changePercentTriggersBuy)
                {
                    foreach (var changePercentTriggerSell in changePercentTriggersSell)
                    {
                        var strategy = new UpDownDiffBuySell($"UpDownDiffBSAmount% ({timeGranularity} - {(changePercentTriggerBuy)}|{(changePercentTriggerSell)})", periods);
                        foreach (var period in periods)
                        {
                            var periodData = new UpDownDPeriodData
                            {
                                TimeGranularity = timeGranularity,
                                ChangePercentTriggerBuy = changePercentTriggerBuy,
                                ChangePercentTriggerSell = changePercentTriggerSell,
                                HoldTime = new TimeSpan(),
                                WaitTime = new TimeSpan(),
                                ReinvestValue = 1,
                                WithdrawProfit = 0,
                                WaitingForDown = true,
                                Trades = new List<Trade>()
                            };
                            strategy._periodData[period.Name] = periodData;
                        }
                        strategies.Add(strategy);
                    }
                }
            }

            return strategies;
        }


        internal override void PeriodInitialize(StrategyPeriod period)
        {
            
        }

        internal override void PeriodUpdate(StrategyPeriod period, int candleIdx, List<Candle> data, int ctr)
        {
            var candle = data[candleIdx];
            var periodData = _periodData[period.Name];

            // check time granularity passed
            if (candleIdx < periodData.IgnoreToIndex) return;

            // wait till we hit the first buy point
            if (periodData.LastBuy == null)
            {
                PeriodBuy(periodData, candle);
                periodData.IgnoreToIndex = candleIdx + periodData.TimeGranularity;
                periodData.FirstBuyDate = candle.Time;
            }
            else
            {
                var lastCompareCandle = data[candleIdx - periodData.TimeGranularity];

                if (periodData.WaitingForDown)
                {
                    if (periodData.ChangeAmountTriggerSell != 0)
                    {
                        // change by amount
                        // time to sell
                        if ((double)lastCompareCandle.Open - (double)candle.Open > periodData.ChangeAmountTriggerSell)
                        {
                            PeriodSell(periodData, candle);
                            periodData.WaitingForDown = false;
                        }
                    } 
                    else
                    {
                        // change by percent
                        // time to sell
                        if (((double)lastCompareCandle.Open - (double)candle.Open)/(double)lastCompareCandle.Open > periodData.ChangePercentTriggerSell)
                        {
                            PeriodSell(periodData, candle);
                            periodData.WaitingForDown = false;
                        }
                    }
                }
                else
                {
                    if (periodData.ChangeAmountTriggerBuy != 0)
                    {
                        // change by amount
                        // time to buy
                        if ((double)candle.Open - (double)lastCompareCandle.Open > periodData.ChangeAmountTriggerBuy)
                        {
                            PeriodBuy(periodData, candle);
                            periodData.WaitingForDown = true;
                        }
                    }
                    else
                    {
                        // change by percent
                        // time to buy
                        if (((double)candle.Open - (double)lastCompareCandle.Open)/(double)lastCompareCandle.Open > periodData.ChangePercentTriggerBuy)
                        {
                            PeriodBuy(periodData, candle);
                            periodData.WaitingForDown = true;
                        }
                    }
                }
            }
        }
        internal override StrategyPeriodResult PeriodFinish(StrategyPeriod period)
        {
            var periodResult = new StrategyPeriodResult();
            var periodData = _periodData[period.Name];
            periodResult.PeriodProfit = periodData.TotalProfit;
            periodResult.PeriodProfitPerSell = periodData.TotalProfit / periodData.SellCount;
            periodResult.PeriodGain = periodData.WithdrawProfit * 100;
            periodResult.PeriodGainCompounding = (periodData.ReinvestValue - 1) * 100;
            periodResult.FirstBuyDate = periodData.FirstBuyDate;
            periodResult.LastSellDate = periodData.LastSell != null ? periodData.LastSell.Time : default(DateTime);
            periodResult.PeriodStartDate = period.PeriodStartDate;
            periodResult.PeriodEndDate = period.PeriodEndDate;
            periodResult.PeriodOpen = period.Stats.PeriodOpen;
            periodResult.PeriodClose = period.Stats.PeriodClose;
            periodResult.Trades = periodData.Trades;
            periodResult.Name = period.Name;
            periodResult.SellCount = periodData.SellCount;
            periodResult.AverageHoldDays = periodData.HoldTime.TotalDays / (period.PeriodEndDate - period.PeriodStartDate).TotalDays;
            periodResult.AverageWaitDays = periodData.WaitTime.TotalDays / (period.PeriodEndDate - period.PeriodStartDate).TotalDays;
            return periodResult;
        }
    }
}
