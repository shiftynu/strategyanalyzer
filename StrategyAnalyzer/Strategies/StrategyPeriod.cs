﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CoinbasePro.Services.Products.Models;

namespace StrategyAnalyzer.Strategies
{
    public class StrategyPeriod
    {
        public StrategyPeriod(string name, DateTime periodStartDate, DateTime periodEndDate)
        {
            Name = name;
            PeriodStartDate = periodStartDate;
            PeriodEndDate = periodEndDate;
        }

        public void InitStats(List<Candle> data)
        {
            Stats = new DataStats(data.Where(c => c.Time >= PeriodStartDate && c.Time <= PeriodEndDate).ToList());
        }

        public override string ToString()
        {
            return $"{Name}: {Stats.StartDate.ToString("MM/dd/yy")} - {Stats.EndDate.ToString("MM/dd/yy")} ({Stats.MinPrice},{Stats.MaxPrice},{Stats.AvgPrice})";
        }

        public DataStats Stats { get; set; }
        public string Name { get; internal set; }
        public DateTime PeriodStartDate { get; private set; }
        public DateTime PeriodEndDate { get; private set; }
    }
}
