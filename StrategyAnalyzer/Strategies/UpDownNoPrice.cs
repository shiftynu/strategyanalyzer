﻿using System;
using System.Collections.Generic;
using CoinbasePro.Services.Products.Models;

namespace StrategyAnalyzer.Strategies
{
    public class UpDownNPPeriodData : PeriodData
    {
        public int TimeGranularity { get; set; }
        public int IgnoreToIndex { get; internal set; }
        public bool WaitingForDown { get; set; }
    }

    public class UpDownNoPriceBuySell : StrategyBase
    {
        private Dictionary<string, UpDownNPPeriodData> _periodData = new Dictionary<string, UpDownNPPeriodData>();

        public UpDownNoPriceBuySell(string strategyName, List<StrategyPeriod> periods) : base(strategyName, periods) { }

        public static List<StrategyBase> BuildStrategies(string productTypeString, List<StrategyPeriod> periods, DataStats allDataStats)
        {
            var strategies = new List<StrategyBase>();
            var timeGranularities = new List<int>();
            //for (int i = 1; i < 30; i+=3) timeGranularities.Add(i);
            for (int i = 1; i < 2880; i++) timeGranularities.Add(i);
            for (int i = 2910; i <= 40320; i += 30) timeGranularities.Add(i);

            // build amount triggers to check
            foreach (var timeGranularity in timeGranularities)
            {
                var strategy = new UpDownNoPriceBuySell($"UpDown ({timeGranularity})", periods);
                foreach (var period in periods)
                {
                    var periodData = new UpDownNPPeriodData
                    {
                        TimeGranularity = timeGranularity,
                        HoldTime = new TimeSpan(),
                        WaitTime = new TimeSpan(),
                        ReinvestValue = 1,
                        WithdrawProfit = 0,
                        WaitingForDown = true,
                        Trades = new List<Trade>()
                    };
                    strategy._periodData[period.Name] = periodData;
                }
                strategies.Add(strategy);
            }

            return strategies;
        }


        internal override void PeriodInitialize(StrategyPeriod period)
        {
            
        }

        internal override void PeriodUpdate(StrategyPeriod period, int candleIdx, List<Candle> data, int ctr)
        {
            var candle = data[candleIdx];
            var periodData = _periodData[period.Name];

            // check time granularity passed
            if (candleIdx < periodData.IgnoreToIndex) return;

            // wait till we hit the first buy point
            if (periodData.LastBuy == null)
            {
                PeriodBuy(periodData, candle);
                periodData.IgnoreToIndex = candleIdx + periodData.TimeGranularity;
                periodData.FirstBuyDate = candle.Time;
            }
            else
            {
                var lastCompareCandle = data[candleIdx - periodData.TimeGranularity];

                if (periodData.WaitingForDown)
                {
                    // change by amount
                    // time to sell
                    if ((double)lastCompareCandle.Open - (double)candle.Open > 0)
                    {
                        PeriodSell(periodData, candle);
                        periodData.WaitingForDown = false;
                    }
                }
                else
                {
                    // change by amount
                    // time to buy
                    if ((double)candle.Open - (double)lastCompareCandle.Open > 0)
                    {
                        PeriodBuy(periodData, candle);
                        periodData.WaitingForDown = true;
                    }
                }
            }
        }
        internal override StrategyPeriodResult PeriodFinish(StrategyPeriod period)
        {
            var periodResult = new StrategyPeriodResult();
            var periodData = _periodData[period.Name];
            periodResult.PeriodProfit = periodData.TotalProfit;
            periodResult.PeriodProfitPerSell = periodData.TotalProfit / periodData.SellCount;
            periodResult.PeriodGain = periodData.WithdrawProfit * 100;
            periodResult.PeriodGainCompounding = (periodData.ReinvestValue - 1) * 100;
            periodResult.FirstBuyDate = periodData.FirstBuyDate;
            periodResult.LastSellDate = periodData.LastSell != null ? periodData.LastSell.Time : default(DateTime);
            periodResult.PeriodStartDate = period.PeriodStartDate;
            periodResult.PeriodEndDate = period.PeriodEndDate;
            periodResult.PeriodOpen = period.Stats.PeriodOpen;
            periodResult.PeriodClose = period.Stats.PeriodClose;
            periodResult.Trades = periodData.Trades;
            periodResult.Name = period.Name;
            periodResult.SellCount = periodData.SellCount;
            periodResult.AverageHoldDays = periodData.HoldTime.TotalDays / (period.PeriodEndDate - period.PeriodStartDate).TotalDays;
            periodResult.AverageWaitDays = periodData.WaitTime.TotalDays / (period.PeriodEndDate - period.PeriodStartDate).TotalDays;
            return periodResult;
        }
    }
}
