﻿using System;
using System.Collections.Generic;
using CoinbasePro.Services.Products.Models;

namespace StrategyAnalyzer.Strategies
{
    public class UpDownMPPeriodPrice : PeriodData
    {
        public int TimeGranularity { get; set; }
        public double ChangePercentTriggerBuy { get; set; }
        public double WalkbackPercentTriggerSell { get; set; }
        public double MinProfitPercent { get; set; }
        public bool WaitingForDown { get; set; }
        public int IgnoreToIndex { get; internal set; }
        public double MaxPriceSinceLastBuy { get; set; }
    }

    public class UpDownDiffMinPrice : StrategyBase
    {
        private Dictionary<string, UpDownMPPeriodPrice> _periodData = new Dictionary<string, UpDownMPPeriodPrice>();

        public UpDownDiffMinPrice(string strategyName, List<StrategyPeriod> periods) : base(strategyName, periods) { }

        public static List<StrategyBase> BuildStrategies(string productTypeString, List<StrategyPeriod> periods, DataStats allDataStats)
        {
            var strategies = new List<StrategyBase>();
            var timeGranularities = new List<int>();
            //for (int i = 1; i < 30; i+=3) timeGranularities.Add(i);
            for (int i = 1; i < 2880; i++) timeGranularities.Add(i);
            for (int i = 2910; i <= 40320; i += 60) timeGranularities.Add(i);
            var changePercentTriggersBuy = new List<double>() { 0.0005, 0.00075, 0.001, 0.003, 0.007 };
            var walkbackPercentTriggersSell = new List<double>() { 0.001, 0.005, 0.01, 0.025, };
            var minProfPercents = new List<double> { 0.01, 0.02, 0.05, 0.10, 0.25, 0.30 };

            // build amount triggers to check
            foreach (var timeGranularity in timeGranularities)
            {
                foreach (var changePercentTriggerBuy in changePercentTriggersBuy)
                {
                    foreach (var walkbackPercentTriggerSell in walkbackPercentTriggersSell)
                    {
                        foreach (var minProfPercent in minProfPercents)
                        {
                            var strategy = new UpDownDiffMinPrice($"UpDownDiffMinProf ({timeGranularity} - {(changePercentTriggerBuy)}|{(walkbackPercentTriggerSell)}|{minProfPercent})", periods);
                            foreach (var period in periods)
                            {
                                var periodData = new UpDownMPPeriodPrice
                                {
                                    TimeGranularity = timeGranularity,
                                    ChangePercentTriggerBuy = changePercentTriggerBuy,
                                    WalkbackPercentTriggerSell = walkbackPercentTriggerSell,
                                    MinProfitPercent = minProfPercent,
                                    HoldTime = new TimeSpan(),
                                    WaitTime = new TimeSpan(),
                                    ReinvestValue = 1,
                                    WithdrawProfit = 0,
                                    WaitingForDown = false,
                                    Trades = new List<Trade>()
                                };
                                strategy._periodData[period.Name] = periodData;
                            }
                            strategies.Add(strategy);
                        }
                    }
                }
            }

            return strategies;
        }


        internal override void PeriodInitialize(StrategyPeriod period)
        {
            
        }

        internal override void PeriodUpdate(StrategyPeriod period, int candleIdx, List<Candle> data, int ctr)
        {
            var candle = data[candleIdx];
            var periodData = _periodData[period.Name];

            // check time granularity passed
            if (candleIdx < periodData.TimeGranularity) return;

            var lastCompareCandle = data[candleIdx - periodData.TimeGranularity];

            if (periodData.WaitingForDown)
            {
                // change by percent
                // time to sell
                var currentProfitPercent = ((double)candle.Open - (double)periodData.LastBuy.Open)/ (double)candle.Open;
                var walkbackPercentOfMaxPrice = periodData.MaxPriceSinceLastBuy * (1 - periodData.WalkbackPercentTriggerSell);
                if (currentProfitPercent > periodData.MinProfitPercent && (double)candle.Open < walkbackPercentOfMaxPrice)
                {
                    PeriodSell(periodData, candle);
                    periodData.WaitingForDown = false;
                } 
                else
                {
                    if ((double)candle.Open > periodData.MaxPriceSinceLastBuy) periodData.MaxPriceSinceLastBuy = (double)candle.Open;
                }
            }
            else
            {
                // change by percent
                // time to buy
                if (((double)candle.Open - (double)lastCompareCandle.Open)/(double)lastCompareCandle.Open > periodData.ChangePercentTriggerBuy)
                {
                    PeriodBuy(periodData, candle);
                    periodData.WaitingForDown = true;
                    periodData.MaxPriceSinceLastBuy = (double)candle.Open;
                }
            }
        }
        internal override StrategyPeriodResult PeriodFinish(StrategyPeriod period)
        {
            var periodResult = new StrategyPeriodResult();
            var periodData = _periodData[period.Name];
            periodResult.PeriodProfit = periodData.TotalProfit;
            periodResult.PeriodProfitPerSell = periodData.TotalProfit / periodData.SellCount;
            periodResult.PeriodGain = periodData.WithdrawProfit * 100;
            periodResult.PeriodGainCompounding = (periodData.ReinvestValue - 1) * 100;
            periodResult.FirstBuyDate = periodData.FirstBuyDate;
            periodResult.LastSellDate = periodData.LastSell != null ? periodData.LastSell.Time : default(DateTime);
            periodResult.PeriodStartDate = period.PeriodStartDate;
            periodResult.PeriodEndDate = period.PeriodEndDate;
            periodResult.PeriodOpen = period.Stats.PeriodOpen;
            periodResult.PeriodClose = period.Stats.PeriodClose;
            periodResult.Trades = periodData.Trades;
            periodResult.Name = period.Name;
            periodResult.SellCount = periodData.SellCount;
            periodResult.AverageHoldDays = periodData.HoldTime.TotalDays / (period.PeriodEndDate - period.PeriodStartDate).TotalDays;
            periodResult.AverageWaitDays = periodData.WaitTime.TotalDays / (period.PeriodEndDate - period.PeriodStartDate).TotalDays;
            return periodResult;
        }
    }
}
